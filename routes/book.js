const express = require("express");
const router = express.Router();
const multer = require("../middlewares/multer");
const bookControllers = require("../controllers/book");
router.post("/add", multer.single("bookImage"), bookControllers.Create);
//router.post("/add", bookControllers.Create);
router.get("/", bookControllers.Read);
//router.put("/edit/:id", bookControllers.Update);
router.put("/edit/:id", multer.single("bookImage"), bookControllers.Update);

router.delete("/delete/:id", bookControllers.Delete);

module.exports = router;

