const mongoose = require('mongoose');
const { Schema } = mongoose;

const bookSchema = new Schema({
    title: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    // author: {
    //     type: String,
    //     required: true,
    //     lowercase: true,
    //     trim: true
    // },
    // publisher: {
    //     type: String,
    //     required: true,
    //     lowercase: true,
    //     trim: true
    // },
    // publication_year: {
    //     type: Number,
    //     //required: true,
    // },
    // cost_price: {
    //     type: Number,
    //     //required: true,
    // },
    // sale_price: {
    //     type: Number,
    //     //required: true,
    // },
    quantity: {
        type: Number,

        required: true,
    },
    bookImage :{
        type:String,
        default:'https://res.cloudinary.com/pt-kita-bangun-indonesia/image/upload/v1607180695/Screenshot_1605714058_b8qgbq.png'
        
    }

}, { timestamps: true, versionKey: false });

const book = mongoose.model("Book", bookSchema);

exports.Book = book;