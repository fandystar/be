const multer = require("multer");
const cloudinary = require("cloudinary").v2;
const { CloudinaryStorage } = require("multer-storage-cloudinary");

// cloud config
cloudinary.config({
    cloud_name: "pt-kita-bangun-indonesia",
    api_key: "254356966726933",
    api_secret: "v05ZuX9TdvkMjrkUj-yWjpBHBWs",
});

// storage
const storage = new CloudinaryStorage({
    cloudinary: cloudinary,
    folder: "books",
    allowedFormats: ["jpg", "jpeg", "png", "svg"],
    filename: (req, files, cb) => {
        cb(null, Date.now() + "_" + files.originalname.split(".")[0]);
    },
});

const uploader = multer({
    storage: storage,
});

module.exports = uploader;